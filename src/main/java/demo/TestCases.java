package demo;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import io.github.bonigarcia.wdm.WebDriverManager;
//Selenium Imports

import org.openqa.selenium.Keys;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;





public class TestCases {
    ChromeDriver driver;
    public TestCases()
    {
        System.out.println("Constructor: TestCases");
        WebDriverManager.chromedriver().timeout(30).setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

    }

    public void endTest()
    {
        System.out.println("End Test: TestCases");
        driver.close();
        driver.quit();

    }

    
    public  void testCase01() throws InterruptedException {
        System.out.println("Start Test case: testCase01");
        driver.get("https://www.imdb.com/chart/top");

        WebElement dropdown = driver.findElement((By.id("sort-by-selector")));
        Select drop_down = new Select(dropdown);
        drop_down.selectByIndex(2);

        WebElement mostRatedMovie = driver.findElement(By.xpath("//*[@id=\"__next\"]/main/div/div[3]/section/div/div[2]/div/ul/li[1]/div[2]/div/div/div/a/h3"));
        System.out.println("The highest rated movie on IMDb: " + mostRatedMovie.getText());

        List<WebElement> allMovies = driver.findElements(By.xpath("//div[@data-testid='chart-layout-parent']/div[2]/div/ul/li"));
        System.out.println("The number of movies included in the table: " + allMovies.size());

        drop_down.selectByIndex(3);
        Thread.sleep(2000);

        WebElement last_movie =driver.findElement(By.xpath("(//*[@id='__next']/main/div/div[3]/section/div/div[2]/div/ul/li/div[2]/div/div/div/a/h3)[last()]"));
        System.out.println("The oldest movie on the list: " + last_movie.getText());

        WebElement recent_movie =driver.findElement(By.xpath("//*[@id='__next']/main/div/div[3]/section/div/div[2]/div/ul/li[1]/div[2]/div/div/div/a/h3"));
        System.out.println("The most recent movie on the list: " + recent_movie.getText());

        drop_down.selectByIndex(4);
        Thread.sleep(2000);
        WebElement most_user_ratings =driver.findElement(By.xpath("//*[@id=\"__next\"]/main/div/div[3]/section/div/div[2]/div/ul/li[1]/div[2]/div/div/div/a/h3"));
        System.out.println("The most user ratings movie on the list: " + most_user_ratings.getText());

        System.out.println("end Test case: testCase02");
    }


}
